/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.cryplogexample;

import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.FileAppender;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.LoggingEvent;
/**
 *
 * @author MXE01022664A
 */
public class EncrypAppender extends FileAppender {

	private boolean initialized = false;
	private String baseFileName = null;
	private int numLines = 0;
	private long startFile = 0;
	private int maxRecords = 0;
	private int maxSec = 0;
	private String dateFormat;
	private String file;
	public static Log log = LogFactory.getLog(EncrypAppender.class);
	private static final Timer timer = new Timer(true);
	
        public static void init(){

            String path = "C:\\kiosco/";
            String timest = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date());
            System.setProperty("output", path+".log/encrypt_Sura_"+timest);
            PropertyConfigurator.configure(path+"Properties/log4j.properties");
            EncrypAppender.log = LogFactory.getLog(EncrypAppender.class);
        }
	/**
	 * 
	 * write to ActivityLog 
	 * 
	 * @param event logging event invoked.
	 *            
	 */
	@Override
	protected void subAppend(LoggingEvent event) {
            String modifiedMessage = Base64.getEncoder().encodeToString(event.getMessage().toString().getBytes());
            LoggingEvent modifiedEvent = new LoggingEvent(event.getFQNOfLoggerClass(), event.getLogger(), event.getTimeStamp(), event.getLevel(), modifiedMessage,
                                                      event.getThreadName(), event.getThrowableInformation(), event.getNDC(), event.getLocationInformation(),
                                                      event.getProperties());
		if(!initialized || checkStatus())
		{
			createNewFile();
		}
		synchronized (this) {
			super.subAppend(modifiedEvent);
			numLines++;
		}
	}
	
	/**
	 * 
	 * create a new ActivityLog File 
	 * 
	 */
	public void createNewFile()
	{	
		try {
			baseFileName = file + "_"+ InetAddress.getLocalHost().getHostAddress()+ "_"+ new SimpleDateFormat(dateFormat).format(Calendar.getInstance().getTime());
			super.setFile(getTmpFileName());
			super.activateOptions();
			startFile = System.currentTimeMillis();
			log.debug("Created Activity Log: " + getTmpFileName());
			startTimer();
			initialized = true;
		} catch (Exception e) {
			log.error("Error in configuration of log4j params,unable to create ActivityLog file");
		}
	}
	
	/**
	 * 
	 * Activity Log status check.
	 * It will be closed as per configured time or configured max. no. of records
	 * 
	 * @return boolean true indicates current activity log file has been closed         
	 */
	private boolean checkStatus()
	{
		boolean flag = false;
		if ((maxSec==0 && numLines==maxRecords) || (maxRecords==0 && System.currentTimeMillis()>=maxSec*1000+startFile)||
			(maxSec!=0 && maxRecords!=0 && (numLines == maxRecords || System.currentTimeMillis()>=maxSec*1000+startFile))) 
		{
			rollOver();			
			flag=true;
		}	
		return flag;
	}
	
	/**
	 * 
	 * invokes File Appender's activateOptions() which controls the creation of log files. 
	 * 
	 */
	@Override
	public void activateOptions()
	{ 
		if (maxSec == 0 && maxRecords == 0) {
			log.info("ActivityLog: Time duration and Max records both are zeroed, not creating AL files");
		} else {
			super.setFile("dummy");
			super.activateOptions();
		}
	}
	
	/**
	 * 
	 * Close and rename the current ActivityLog file and reset counter and timestamp.
	 * 	         
	 */
	private void rollOver() {	
		
		    log.debug("Closing and Renaming Activity Log File: " + getTmpFileName());
			closeFile();			
			(new File(getTmpFileName())).renameTo(new File(getReportFileName()));
			numLines = 0;
			startFile=0;
			initialized = false;			
	}

	private String getReportFileName() {
		return baseFileName+".report";
	}

	private String getTmpFileName() {
		return baseFileName+".tmp";
	}
	
	public void setMaxRecords(int maxRecords) {
		this.maxRecords = maxRecords;
	}

	public void setMaxSec(int maxSec) {
		this.maxSec = maxSec;
	}
	
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}	
	
	@Override
	public void setFile(String file) {
			this.file=file;		
	}
	/**
	 * 
	 * start the timer to monitor the time duration
	 *            
	 */
	private void startTimer() {
		if (maxSec != 0) {
			TimerTask timertask = new MonitorTime();
			timer.schedule(timertask, maxSec * 1000);
		}
	}
	/**
	 * 
	 * Private inner class,will monitor time duration and close the 
	 * Activity Log file if the configured time has elapsed
	 * 	         
	 */
	private class MonitorTime extends TimerTask {

		public void run() {
			// time over
			if (System.currentTimeMillis()>= maxSec * 1000 + startFile) {
				rollOver();

			}
		}
	}
	
}