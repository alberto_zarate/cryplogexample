package com.example.cryplogexample;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.apache.log4j.Priority;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        EncrypAppender.log.error("Hola mundo");
        EncrypAppender.log.info("Oye mundo");
        EncrypAppender.log.warn("Cuidado mundo");
        EncrypAppender.log.debug("Test mundo");
        EncrypAppender.log.fatal("Oh nooo! mundo");
        EncrypAppender.log.trace("Buscando a mundo");
    }    
}
